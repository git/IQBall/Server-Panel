using AdminPanel;
using AdminPanel.Services;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using MudBlazor.Services;

var builder = WebAssemblyHostBuilder.CreateDefault(args);
builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");
builder.Services
    .AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });

// builder.Logging.SetMinimumLevel(LogLevel.Debug);

var client = new HttpClient
{
    BaseAddress = new Uri("http://localhost:5254")
};

builder.Services.AddScoped<IUsersService>(sp => new HttpUsersService(client));
//builder.Services.AddScoped<IUsersService, UsersServiceStub>();
builder.Services.AddScoped<ITeamService>(sp => new HttpTeamService(client));

builder.Services.AddMudServices();


await builder.Build().RunAsync();

