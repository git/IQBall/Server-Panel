using System.Net.Http.Json;
using AdminPanel.Models;

namespace AdminPanel.Services;

public class HttpTeamService : ITeamService
{
    private readonly HttpClient _client;

    public HttpTeamService(HttpClient client)
    {
        this._client = client;
    }

    private record CountTeamsResponse(uint Value);

    public async Task<(uint, List<Team>)> ListTeam(uint from, uint count)
    {
        var httpResponse = await _client.GetAsync($"/admin/teams?start={from}&n={count}");
        httpResponse.EnsureSuccessStatusCode();
        var teams = (await httpResponse.Content.ReadFromJsonAsync<List<Team>>())!;

        httpResponse =
            await _client.GetAsync($"/admin/teams/count");
        await ErrorsUtils.EnsureResponseIsOk(httpResponse);

        var countResponse = await httpResponse.Content.ReadFromJsonAsync<CountTeamsResponse>();

        return (countResponse!.Value, teams);
    }

    private record AddTeamRequest(string Name, string Picture, string FirstColor, string SecondColor);

    public async Task AddTeam(string name, string picture, string mainColor, string secondaryColor)
    {
        var httpResponse = await _client.PostAsJsonAsync($"/admin/teams",
            new AddTeamRequest(name, picture, mainColor, secondaryColor));
        await ErrorsUtils.EnsureResponseIsOk(httpResponse);
    }

    private record DeleteTeamsRequest(List<uint> Teams);

    public async Task DeleteTeams(List<uint> teams)
    {
        var httpResponse = await _client.PostAsJsonAsync($"/admin/teams/remove-all", new DeleteTeamsRequest(teams));
        await ErrorsUtils.EnsureResponseIsOk(httpResponse);
    }

    private record UpdateTeamRequest(uint Id, string Name, string Picture, string MainColor, string SecondaryColor);

    public async Task UpdateTeam(Team team)
    {
        var httpResponse = await _client.PutAsJsonAsync($"/admin/teams/{team.Id}",
            new UpdateTeamRequest(team.Id, team.Name, team.Picture, team.MainColor, team.SecondColor));
        await ErrorsUtils.EnsureResponseIsOk(httpResponse);
    }
}