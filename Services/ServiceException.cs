using System.Net;
using System.Net.Http.Json;

namespace AdminPanel.Services;

public class ServiceException : Exception
{
    public Dictionary<string, List<string>> ArgumentMessages { get; init; }

    
    
    public ServiceException(string? message, Dictionary<string, List<string>> arguments) : base(message)
    {
        ArgumentMessages = arguments;
    }

    public ServiceException(string? message, Dictionary<string, List<string>> arguments, Exception? innerException) : base(message, innerException)
    {
        ArgumentMessages = arguments;
    }
    
    
}