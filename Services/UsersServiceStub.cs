﻿using AdminPanel.Models;

namespace AdminPanel.Services
{
    public class UsersServiceStub : IUsersService
    {
        private Dictionary<uint, User> Users { get; } = new[]
        {
            new User
            {
                Name = "Mathis",
                Email = "mathis@gmail.com",
                Id = 0,
                IsAdmin = true
            },
            new User
            {
                Name = "Maeva",
                Email = "maeva@gmail.com",
                Id = 1,
                IsAdmin = false
            },
        }.ToDictionary(u => u.Id);

        public Task<User> AddUser(string username, string email, string password, bool isAdmin)
        {
            User user = new User
            {
                Email = email,
                Name = username,
                IsAdmin = isAdmin,
                Id = (uint) Users.Count
            };
            Users[user.Id] = user;
            return Task.FromResult(user);
        }

        public async Task<(uint, List<User>)> ListUsers(uint from, uint len, string? searchString = null)
        {
            //simulate a 1sec long request
            await Task.Delay(1000);
            var slice = Users.Values
                .ToList()
                .FindAll(a => searchString == null || a.Name.Contains(searchString) || a.Email.Contains(searchString))
                .GetRange((int)from, (int)(from + len > Users.Count ? Users.Count - from : len));

            return ((uint)Users.Count, slice);
        }

        public Task RemoveUsers(IEnumerable<uint> userIds)
        {
            foreach (var id in userIds)
            {
                Users.Remove(id);
            }
            return Task.CompletedTask;
        }

        public Task UpdateUser(User user)
        {
            Users[user.Id] = user;
            return Task.CompletedTask;
        }
    }
}