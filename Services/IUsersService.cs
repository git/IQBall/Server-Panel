﻿using AdminPanel.Models;

namespace AdminPanel.Services
{
    public interface IUsersService
    {
        public Task<(uint, List<User>)> ListUsers(uint from, uint len, string? searchString = null);

        public Task<User> AddUser(string username, string email, string password, bool isAdmin);

        public Task RemoveUsers(IEnumerable<uint> userId);

        public Task UpdateUser(User user);
    }
}
