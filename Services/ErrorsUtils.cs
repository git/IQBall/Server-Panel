using System.Net;
using System.Net.Http.Json;

namespace AdminPanel.Services;

public class ErrorsUtils
{
    private record ServerErrorMessageDto(string? Field, string? Error, string Message);

    public static async Task EnsureResponseIsOk(HttpResponseMessage response)
    {
        if (response.StatusCode == HttpStatusCode.OK)
        {
            return;
        }
        
        var content = await response.Content.ReadFromJsonAsync<ServerErrorMessageDto[]>();
        var messages = content!
            .GroupBy(e => e.Field ?? e.Error!)
            .ToDictionary(
                g => g.Key,
                g => g.ToList().ConvertAll(e => e.Message)
            );

        throw new ServiceException("Server refused request", messages);
    }
}