using AdminPanel.Models;

namespace AdminPanel.Services;

public interface ITeamService
{
    public Task<(uint, List<Team>)> ListTeam(uint from, uint count);
    public Task AddTeam(string name, string picture, string mainColor, string secondaryColor);
    public Task  DeleteTeams(List<uint> teams);
    public Task UpdateTeam(Team team);

}