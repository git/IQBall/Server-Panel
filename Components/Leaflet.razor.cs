﻿using Microsoft.AspNetCore.Components;

namespace AdminPanel.Components
{
    public partial class Leaflet
    {
        
        [Parameter]
        public RenderFragment? Header { get; init; }
        
        [Parameter]
        public RenderFragment? Body { get; init; }

        [Parameter] 
        public bool Expanded { get; set; } = false;

        private void OnClickHeader()
        {
            Expanded = !Expanded;
        }
        
    }
}
