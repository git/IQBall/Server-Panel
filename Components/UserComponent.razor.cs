﻿using AdminPanel.Models;
using Microsoft.AspNetCore.Components;

namespace AdminPanel.Components
{
    public partial class UserComponent
    {
        [Parameter]
        public User User { get; init; }

    }
}
