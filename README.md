
# instructions
Avant de démarrer cette application, il faut tout d'abord démarrer le serveur : 

```bash
git clone https://codefirst.iut.uca.fr/git/IQBall/Appliction-Web.git
cd Appliction-Web
dotnet run --project API 
```

puis, dans un second terminal, démarrer ce projet :
```
git clone https://codefirst.iut.uca.fr/git/IQBall/Server-Panel
cd Server-Panel
dotnet run
```


