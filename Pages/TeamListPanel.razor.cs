using System.ComponentModel.DataAnnotations;
using System.Text;
using AdminPanel.Models;
using AdminPanel.Services;
using Blazorise.Extensions;
using Microsoft.AspNetCore.Components;
using MudBlazor;

namespace AdminPanel.Pages;

public partial class TeamListPanel
{
    [Inject] private ISnackbar Snackbar { get; init; }
    [Inject] private ITeamService TeamService { get; init; }
    private bool _isOpenAdd;
    private bool _isOpenConf;
    private bool _success;
    private string[] _errors = { };
    
    [StringLength(1,ErrorMessage = "Nombre de caractères minimal : 1 ")] 
    private string? FormName { get; set; }
    
    [RegularExpression(@"(https:\/\/www\.|http:\/\/www\.|https:\/\/|http:\/\/)?[a-zA-Z0-9]{2,}\.[a-zA-Z0-9]{2,}\.[a-zA-Z0-9]{2,}(\.[a-zA-Z0-9]{2,})?", ErrorMessage = "L'image doit être le lien d'une image sur le web")]
    private string? FormPicture { get; set; }

    [RegularExpression(@"(/#[0-9a-f]{6})",ErrorMessage = "La couleur principale doit être un code hexadécimal")]
    private string? FormMainColor { get; set; }
    
    [RegularExpression(@"(/#[0-9a-f]{6})",ErrorMessage = "La couleur secondaire doit être un code hexadécimal")]
    private string? FormSecondaryColor { get; set; }
    private MudDataGrid<Team>? Grid { get; set; }
    private HashSet<Team> SelectedTeams { get; set; } = new();

    private void UnBindForm()
    {
        FormName = null;
        FormPicture = null;
        FormMainColor = null;
        FormSecondaryColor = null;
    }

    private async Task<GridData<Team>> GetTeamsFromServer(GridState<Team> state)
    {
        var (count, teams) = await TeamService.ListTeam((uint)(state.Page * state.PageSize), (uint)state.PageSize);
        return new GridData<Team>
        {
            TotalItems = (int)count,
            Items = teams
        };
    }

    private void ToggleOpen()
    {
        _isOpenAdd = !(_isOpenAdd);
    }

    private void ToggleOpenConfirmation()
    {
        _isOpenConf = !(_isOpenConf);
    }

    private async Task AddTeam()
    {
        try
        {
            await TeamService.AddTeam(FormName!, FormPicture!, FormMainColor!, FormSecondaryColor!);
        }
        catch (ServiceException err)
        {
            DisplayUtils.ShowErrors(err,Snackbar);
        }
    }

    private async void AddTeamConfirmed()
    {
        await AddTeam();
        ToggleOpen();
        await Grid!.ReloadServerData();
        UnBindForm();
    }

    private async void DeleteSelectedTeams()
    {
        if (!SelectedTeams.IsNullOrEmpty())
        {
            var ids = SelectedTeams.ToList().ConvertAll(team => team.Id);
            await TeamService.DeleteTeams(ids);
            await Grid!.ReloadServerData();
        }
        CloseToggleConf();
    }

    private void CloseToggleConf()
    {
        _isOpenConf = false;
    }

    private async void OnTeamUpdated(Team t)
    {
        try
        {
            await TeamService.UpdateTeam(t);
        }
        catch (ServiceException err)
        {
            DisplayUtils.ShowErrors(err,Snackbar);
        }
    }
}