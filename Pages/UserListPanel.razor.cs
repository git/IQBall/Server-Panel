﻿using System.Text;
using AdminPanel.Models;
using AdminPanel.Services;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.Extensions.Primitives;
using MudBlazor;

namespace AdminPanel.Pages
{
    public partial class UserListPanel
    {
        [Inject] private ISnackbar Snackbar { get; init; }
        [Inject] private IUsersService Service { get; init; }
        [Inject] private ILogger<UserListPanel> Logger { get; init; }

        private MudDataGrid<User> Grid { get; set; }


        private string? SearchString { get; set; }


        private HashSet<User> SelectedItems { get; set; } = new();

        private string? FormAccountName { get; set; }
        private string? FormAccountEmail { get; set; }
        private string? FormAccountPassword { get; set; }
        private bool FormAccountIsAdmin { get; set; }

        public bool IsAddingUser { get; set; }


        private async Task<GridData<User>> LoadUsersFromServer(GridState<User> state)
        {
            Logger.LogDebug($"Loading users from server, state = {state} searchString = {SearchString}");
            var (count, users) =
                await Service.ListUsers((uint)(state.Page * state.PageSize), (uint)state.PageSize, SearchString);
            return new GridData<User>
            {
                TotalItems = (int)count,
                Items = users
            };
        }


        private async void OnAccountUpdated(User user)
        {
            Logger.LogDebug($"Account updated : {user}");
            try
            {
                await Service.UpdateUser(user);
            }
            catch (ServiceException err)
            {
                DisplayUtils.ShowErrors(err,Snackbar);
            }
        }

        private async void ConfirmAddAccount(MouseEventArgs e)
        {
            // We no longer add an account if it is confirmed
            IsAddingUser = false;

            try
            {
                var user = await Service.AddUser(FormAccountName!, FormAccountEmail!, FormAccountPassword!,
                    FormAccountIsAdmin);
                Logger.LogDebug($"Added user : {user}");
                await Grid.ReloadServerData();
            }
            catch (ServiceException err)
            {
                DisplayUtils.ShowErrors(err,Snackbar);
            }
        }
        

        private async void RemoveSelection(MouseEventArgs e)
        {
            var users = SelectedItems.ToList().ConvertAll(u => u.Id);
            Logger.LogDebug($"Removing users : {users}");
            try
            {
                await Service.RemoveUsers(users);
                await Grid.ReloadServerData();
            }
            catch (ServiceException err)
            {
                DisplayUtils.ShowErrors(err,Snackbar);
            }
        }

        private Func<string, string?> VerifyLength(uint min, uint max)
        {
            return s => s.Length >= min && s.Length <= max
                ? null
                : $"length is incorrect (must be between {min} and {max})";
        }

        private void ValidateSearch(KeyboardEventArgs e)
        {
            Grid.ReloadServerData();
            Logger.LogDebug($"Searching for {SearchString}");
        }
    }
}