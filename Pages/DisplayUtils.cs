using System.Text;
using AdminPanel.Services;
using MudBlazor;

namespace AdminPanel.Pages;

public class DisplayUtils
{
    public static void ShowErrors(ServiceException e, ISnackbar snackbar)
    {
        foreach (var erronedArgument in e.ArgumentMessages)
        {
            var sb = new StringBuilder(erronedArgument.Key);
            sb.Append(" :");

            foreach (var message in erronedArgument.Value)
            {
                sb.Append("\n\t-");
                sb.Append(message);
            }
            snackbar.Add(sb.ToString());
        }
    }
}