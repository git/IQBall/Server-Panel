﻿namespace AdminPanel.Models;

public class User
{
    public required string Name { get; set; }
    public required string Email { get; set; }
    public uint Id { get; set; }
    public bool IsAdmin { get; set; }

    public override string ToString()
    {
        return $"{nameof(Name)}: {Name}, {nameof(Email)}: {Email}, {nameof(Id)}: {Id}, {nameof(IsAdmin)}: {IsAdmin}";
    }
}