namespace AdminPanel.Models;

public record Team(uint Id, string Name, string Picture, string MainColor, string SecondColor)
{
    
}